<?php 
// Connect to MySQL
function mysqlConnect() {
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "cobalaravel";
    return mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
}

// MySQL Select Data
function getTableStudents() {
    $link = mysqlConnect();
    $query = "SELECT * FROM students";
    $result = mysqli_query($link, $query);

    // Select Data
    $hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $hasil;
}

// echo json_encode(getTableStudents());
?>