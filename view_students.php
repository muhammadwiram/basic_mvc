<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="styles.css">
        <title>Students Table</title>
    </head>
    <body>
        <div class="container">
            <h1>Students Data</h1>
            <table border="1">
                <tr>
                    <th>Name</th>
                    <th>NRP</th>
                    <th>Major</th>
                </tr>
                <?php foreach($studentsData as $data): ?>
                <tr>
                    <td><?php echo $data['name'] ?></td>
                    <td><?php echo $data['nrp'] ?></td>
                    <td><?php echo $data['major'] ?></td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </body>
</html>